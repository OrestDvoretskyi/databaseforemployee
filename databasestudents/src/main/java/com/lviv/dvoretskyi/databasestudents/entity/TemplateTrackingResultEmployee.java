/*
 * Copyright (c) 2018.
 */

package com.lviv.dvoretskyi.databasestudents.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "templateTrackingResultEmployee")
public class TemplateTrackingResultEmployee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne
  private TemplateTrackingResult templateTrackingResult;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public TemplateTrackingResult getTemplateTrackingResult() {
    return templateTrackingResult;
  }

  public void setTemplateTrackingResult(
      TemplateTrackingResult templateTrackingResult) {
    this.templateTrackingResult = templateTrackingResult;
  }
}
