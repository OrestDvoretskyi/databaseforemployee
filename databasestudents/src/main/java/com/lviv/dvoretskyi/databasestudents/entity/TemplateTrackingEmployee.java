package com.lviv.dvoretskyi.databasestudents.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TemplateTrackingEmployee {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;


  @OneToMany
  private Template template;


}
