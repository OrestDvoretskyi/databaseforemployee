/*
 * Copyright (c) 2018.
 */

package com.lviv.dvoretskyi.databasestudents.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "templateTrackingResult")
public class TemplateTrackingResult {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)

  private int id;
  @ManyToOne
  private TemplateTrackingEmployee templateTrackingEmployee;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public TemplateTrackingEmployee getTemplateTrackingEmployee() {
    return templateTrackingEmployee;
  }

  public void setTemplateTrackingEmployee(
      TemplateTrackingEmployee templateTrackingEmployee) {
    this.templateTrackingEmployee = templateTrackingEmployee;
  }
}
